# Install base programs
cd ~/Downloads
https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/CodeNewRoman.zip
unzip CodeNewRoman.zip
rm CodeNewRoman.zip
rm *Windows\ Compatible.otf

mkdir ~/.local/share/fonts
mv ~/Downloads/Code* ~/.local/share/fonts
sudo fc-cache -f -v
cd ..

sudo apt install build-essential fish python3-venv python-is-python3 libssl-dev -y
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt install snapd -y
snap install micro --classic
NPM_CONFIG_PREFIX=~/.joplin-bin npm install -g joplin
sudo ln -s ~/.joplin-bin/bin/joplin /usr/bin/joplin
wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash
curl https://sh.rustup.rs -sSf | sh  
source $HOME/.cargo/env
cargo install nu exa starship

sudo add-apt-repository ppa:mmstick76/alacritty  
sudo apt install alacritty -y

git clone --bare https://gitlab.com/Endominus/config.git $HOME/.myconfigs 
alias config='/usr/bin/git --git-dir=$HOME/.myconfigs/ --work-tree=$HOME'  
config checkout  
mkdir -p .config-backup && config checkout 2>&1 | egrep "\\s+." | awk {'print $1'} | xargs dirname | xargs -I{} mkdir -p {} .config-backup/{} 
config checkout 2>&1 | egrep "\\s+." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{} 
config checkout  
config config --local status.showUntrackedFiles no  

# Final settings
gsettings set org.gnome.desktop.background picture-uri ~/Pictures/wallpapers/Shodan_ASCII.png  
cp -r ~/.config/omf/themes/spyral ~/.local/share/omf/themes/
rm ~/.config/fish/functions/fish_prompt.fish
joplin mkbook scrap
joplin sync --target 3
chsh -s /usr/bin/fish

sudo apt install hledger zathura mcomix clementine figlet tldr cmatrix -y
