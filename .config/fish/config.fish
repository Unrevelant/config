set -gx EDITOR micro
set -x LESS "-RSM~gIsw"
set -gx LESS_TERMCAP_mb (set_color -o red)
set -gx LESS_TERMCAP_md (set_color -o red)
set -gx LESS_TERMCAP_me (set_color normal)
set -gx LESS_TERMCAP_se (set_color normal)
set -gx LESS_TERMCAP_so (set_color -b blue -o yellow)
set -gx LESS_TERMCAP_ue (set_color normal)
set -gx LESS_TERMCAP_us (set_color -o green)
set -x PIP_REQUIRE_VIRTUALENV 0
set -x VIRTUAL_ENV_DISABLE_PROMPT 1

set PATH $PATH "$HOME/.cargo/bin"

abbr -a gs "git status"
abbr -a gc "git commit -m"
abbr -a gp "git push"
abbr -a ga "git add"
abbr -a weather "curl wttr.in"

alias ls="exa -lah --group-directories-first"
alias upgrade="sudo apt update && sudo apt upgrade"
alias dlm="youtube-dl -x --audio-format mp3 -o '~/Music/%(title)s.%(ext)s'"
alias config='/usr/bin/git --git-dir=$HOME/.myconfigs/ --work-tree=$HOME'
config config --local status.showUntrackedFiles no

# fortune | cowsay
